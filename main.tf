terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "eu-central-1"
}

resource "aws_vpc" "example_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "example_subnet_1" {
  vpc_id = aws_vpc.example_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-central-1a"
}

resource "aws_subnet" "example_subnet_2" {
  vpc_id = aws_vpc.example_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-central-1b"
}

resource "aws_internet_gateway" "example_igw" {
  vpc_id = aws_vpc.example_vpc.id
}

resource "aws_route_table" "example_rt" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_igw.id
  }
}

resource "aws_instance" "example_instance" {
  ami = "ami-0ce5fed0588cd9afe"
  instance_type = "t4g.small"
  subnet_id = aws_subnet.example_subnet_1.id
}

resource "aws_db_subnet_group" "example_rds_subnet_group" {
  name = "example_rds_subnet_group"
  subnet_ids = [aws_subnet.example_subnet_1.id, aws_subnet.example_subnet_2.id]
}

resource "aws_db_instance" "example_rds" {
  instance_class = "db.t4g.micro"
  allocated_storage = "20"
  engine = "postgres"
  db_name = "example"
  username = "root"
  password = "Test1234"
  db_subnet_group_name = aws_db_subnet_group.example_rds_subnet_group.name
  skip_final_snapshot = true
}

resource "aws_s3_bucket" "example_bucket" {
  bucket = "example-terraform-bucket"
}
