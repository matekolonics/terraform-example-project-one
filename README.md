# CDK Example Project - Simple
This is a simple webapp infrastructure implemented in Terraform. \
Resources defined in this stack:
- EC2 instance (server for running code)
- RDS instance (database)
- S3 bucket (for storing files)

You can find the more advanced version [here](https://gitlab.com/matekolonics/terraform-example-project-two).
# Working with Terraform
## Creating a new Terraform Project
### 1. Install Terraform
Please see the [official guide](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-terraform).
### 2. Create project directory
```bash
mkdir cdk-example-project
cd cdk-example-project
```
### 4. Create Terraform file
```bash
touch main.tf
```
 ## Deploying using Terraform CLI
Run the following command to install the required plugins (e.g. AWS provider plugin):
 ```bash
terraform init
 ``` 
 Then, deploy your project:
 ```bash
 terraform apply
 ```
